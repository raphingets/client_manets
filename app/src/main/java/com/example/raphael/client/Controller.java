package com.example.raphael.client;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by ak92790 on 2016-06-07.
 */
public class Controller extends AsyncTask<String, String, Music> {

    private URL _url;

    @Override
    protected Music doInBackground(String... params) {

        HttpURLConnection urlConnection;


        StringBuilder result = new StringBuilder();
        StringBuilder responseOutput = new StringBuilder();

        Music Music = new Music();
        try {
            _url = new URL(params[0]);

          //  URL url = URL //new URL("http://10.192.199.177:8080/");
            HttpURLConnection hc = (HttpURLConnection) _url.openConnection();
            hc.setRequestMethod("GET");
            int responseCode = hc.getResponseCode();

            if (responseCode == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader(hc.getInputStream()));
                String line = "";


                while ((line = br.readLine()) != null) {
                    responseOutput.append(line);
                    JSONObject jsonObject = new JSONObject(line);

                        Music.Album = jsonObject.getString("album");
                        Music.Artist = jsonObject.getString("artist");
                        Music.Composer = jsonObject.getString("composer");
                        Music.Genre = jsonObject.getString("genre");
                        Music.Path = jsonObject.getString("path");
                        Music.Title = jsonObject.getString("title");
                        Music.Id = jsonObject.getInt("id");

                }
                br.close();


            }
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return Music;
    }





    }
