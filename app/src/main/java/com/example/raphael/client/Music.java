package com.example.raphael.client;

import android.media.MediaPlayer;

import java.util.concurrent.ExecutionException;

/**
 * Created by Raphael on 2016-06-13.
 */
public class Music {

    public String Album;
    public String Artist;
    public String Composer;
    public String Genre;
    public int Id;
    public String Path;
    public String Title;
    public State State;

    public void SetState(State state)
    {
        this.State = state;
    }
    public State GetState()
    {
        return this.State;
    }

    public Music()
    {
        this.State = com.example.raphael.client.State.Stop;
        this.Title = "Welcome";
    }

}



