package com.example.raphael.client;

/**
 * Created by Raphael on 2016-06-13.
 */
public enum State {
    Play, Pause, Stop
}
