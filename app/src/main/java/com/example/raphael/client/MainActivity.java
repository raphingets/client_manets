package com.example.raphael.client;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.raphael.client.Helpers.FontManager;
import com.example.raphael.client.databinding.MusicplayerBinding;

public class MainActivity extends AppCompatActivity {

    MusicplayerBinding binding;
    Music music;
    Player mediaPlayer;

    TextView txtStream;
    TextView txtLoop;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.musicplayer);

        binding = DataBindingUtil.setContentView(this, R.layout.musicplayer);

        Typeface iconFont = FontManager.getTypeface(getApplicationContext(), FontManager.FONTAWESOME);
        FontManager.markAsIconContainer(findViewById(R.id.icons_container), iconFont);

        txtStream= (TextView) findViewById(R.id.stream);
        txtLoop= (TextView) findViewById(R.id.loop);

        music = new Music();
        mediaPlayer = new Player();
        binding.setMusic(music);
    }

    public void Play(View v) {
        music = mediaPlayer.Play();
        binding.setMusic(music);
    }


    public void Pause(View v){
       mediaPlayer.Pause();
    }
    public void Stop(View v){
        mediaPlayer.Stop();
        binding.setMusic(music = new Music());
    }
    public void StepBackward(View v){
        music = mediaPlayer.StepBackward();
        binding.setMusic(music);
    }
    public void StepForward(View v){
        music = mediaPlayer.StepForward();
        binding.setMusic(music);
    }
    public void Random(View v){
        music = mediaPlayer.Random();
        binding.setMusic(music);
    }

    public void Loop(View v){
        mediaPlayer.Loop();

        if(mediaPlayer.ManageStreaming()) {
            txtLoop.setTextColor(Color.RED);
        }
        else{
            txtLoop.setTextColor(Color.BLACK);
        }
    }
    public void Stream(View v){
        if(mediaPlayer.ManageStreaming()) {
            txtStream.setTextColor(Color.RED);
        }
        else{
            txtStream.setTextColor(Color.BLACK);
        }
    }
    public void Settings(View v)
    {
        Fragment t = new SettingsFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(android.R.id.content, t);
        transaction.addToBackStack(null);
        transaction.commit();
        getFragmentManager().addOnBackStackChangedListener(updatePlayerConnection());
    }

    private FragmentManager.OnBackStackChangedListener updatePlayerConnection()
    {
        FragmentManager.OnBackStackChangedListener result = new FragmentManager.OnBackStackChangedListener()
        {
            public void onBackStackChanged()
            {
                SharedPreferences sharedPref= PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                MainActivity.this.mediaPlayer.url = "http://"+(sharedPref.getString("pref_ip", "") +":"+ sharedPref.getString("pref_port", ""))+"/";
            }
        };
        return result;
    }


    /*
    public void StepBackward(View v){
        music = new Player(music).Play();
        System.out.println(music);

        try {
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource("http://184.162.241.154:8080"+music.Path);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void StepForward(View v){
        music = new Player(music).Play();
        System.out.println(music);

        try {
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource("http://184.162.241.154:8080"+music.Path);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}
