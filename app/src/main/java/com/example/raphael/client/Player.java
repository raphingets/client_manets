package com.example.raphael.client;

import android.media.AudioManager;
import android.media.MediaPlayer;

import com.example.raphael.client.Helpers.ActionType;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * Created by Raphael on 2016-06-13.
 */
public class Player extends MediaPlayer{

    public Music _music;
    public boolean isStreaming;
    public boolean isLoop;
    public String url;
    private String ipAdr;


    public Player()
    {
       _music = new Music();
        this.isStreaming = false;
        this.isLoop = false;

    }

    private String GetUrl()
    {
        if(this.isStreaming)
        {
            return "stream/";
        }
        else
        {
            return "action/";
        }
    }

    private void StartPlayer() {

        try {
            this.reset();
            this.setAudioStreamType(AudioManager.STREAM_MUSIC);
            System.out.print(url);
            this.setDataSource( "url"+ this._music.Path);
            this.prepare();
            this.start();
            this._music.SetState(State.Play);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Music Play()
    {
        try
        {
            if(_music.GetState().equals(State.Stop))
            {
                this._music = new Controller().execute(url+GetUrl() + "play").get();
                if(isStreaming) {
                    this.StartPlayer();
                }
            }
            else if(_music.GetState().equals(State.Pause))
            {
                if(isStreaming) {
                    this.start();
                }
                this._music = new Controller().execute(url+GetUrl() + "play").get();
                return this._music;
            }

            this._music.SetState(State.Play);
            return this._music;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Music();
    }

    public void Pause()
    {
        try
        {
            this._music.SetState(State.Pause);
                new Controller().execute(url+GetUrl()+"pause").get();


                if(isStreaming)
                this.pause();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void Stop()
    {
        try
        {

                this._music.State = State.Stop;
                new Controller().execute(url+GetUrl()+"stop").get();
                if(isStreaming)
                this.stop();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Music StepForward()
    {
        try
        {
            this._music =  new Controller().execute(url+GetUrl()+"next").get();
            if(isStreaming)
            this.StartPlayer();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this._music;
    }

    public Music StepBackward()
    {
        try
        {
            this._music =  new Controller().execute(url+GetUrl()+"previous").get();
            if(isStreaming)
            this.StartPlayer();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return this._music;
    }

    public Music Random()
    {
        try
        {
            this._music =  new Controller().execute(url+GetUrl()+"random").get();
            if(isStreaming)
            this.StartPlayer();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return this._music;
    }

    public void Loop()
    {
        try
        {
            new Controller().execute(GetUrl()+"loop").get();
            this.setLooping(ManageLoop());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public boolean ManageLoop()
    {
        if(this.isLoop == true)
            this.isLoop =false;
        else
            this.isLoop = true;
        return this.isLoop;
    }


    public boolean ManageStreaming()
    {
        if(this.isStreaming == true)
            this.isStreaming =false;
        else
            this.isStreaming = true;
        return this.isStreaming;
    }

}
